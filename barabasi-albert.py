import matplotlib.pyplot as plt
import networkx

n = 1000
m = 1
tries = 1000

data = [0 for i in range(n)]
for t in range(tries):
    g = networkx.barabasi_albert_graph(n, m)
    hist = [0 for i in range(n)]
    for i in range(n):
        hist[g.degree(i)] += 1
    data = [data[i] + hist[i] for i in range(n)]

data = [data[i]*0.1/tries for i in range(n)]

plt.clf()
plt.loglog(range(n), data, 'b')
plt.xlabel('degree')
plt.ylabel('# node')
plt.grid(True)
#plt.axis([0, 50, 0, 7])
#plt.title('d = %d' % d)
#plt.savefig(output + '/d%d_degree-numOfNodes.png' % d)
plt.show()
