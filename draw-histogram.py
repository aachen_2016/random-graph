import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import re

n = int(sys.argv[1])
d = int(sys.argv[2])

filename = "attachment_vertex_degree_n%d_d%d" % (n, d)
data_1 = [[0 for i in range(n)] for j in range(n)]
count = 0
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("output/" + f) as textFile:
                for line in textFile:
                    count += 1
                    degree = line.split()
                    for i in range(n):
                        data_1[i][int(degree[i])] += 1
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

if count > 0:
    data_1 = [[data_1[i][j]*1.0/count for j in range(n)] for i in range(n)]

filename = "barabasi_vertex_degree_n%d_d%d" % (n, d)
data_2 = [[0 for i in range(n)] for j in range(n)]
count = 0
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("output/" + f) as textFile:
                for line in textFile:
                    count += 1
                    degree = line.split()
                    for i in range(n):
                        data_2[i][int(degree[i])] += 1
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

if count > 0:
    data_2 = [[data_2[i][j]*1.0/count for j in range(n)] for i in range(n)]

output = './degree_distribution/n%d' % n
if not os.path.exists(output):
    os.mkdir(output)

'''
for i in range(0, n, n/10):    
    plt.clf()
    plt.plot(range(n), data[i], 'b')
    plt.plot(range(n), data_math[i], 'r')
    plt.legend(['experiment', 'math form'], loc='upper right')
    plt.xlabel('degree')
    plt.ylabel('probability')
    plt.grid(True)
    plt.axis([0, max_x+2, 0, max_y])
    plt.title('d = %d, vertex = %d' % (d, i+1))
    plt.savefig(output + '/d%d_vertex_%d.png' % (d, i+1))
'''

#draw expected degree of vertex: sum(x*P(x))
#math form of vertex k: d * ((k-1)/k + sum_(j=k+1->n): 1/j)
expected_degree = [[-1 for i in range(n)] for j in range(3)]
for i in range(n):
    sum = 0
    for j in range(n):
        sum += data_1[i][j]
        if sum > 0.1 and expected_degree[0][i] == -1:
            expected_degree[0][i] = j
        elif sum > 0.5 and expected_degree[1][i] == -1:
            expected_degree[1][i] = j
        elif sum > 0.9 and expected_degree[2][i] == -1:
            expected_degree[2][i] = j

plt.plot(range(1, n+1), expected_degree[0], 'r')
plt.plot(range(1, n+1), expected_degree[1], 'g')
plt.plot(range(1, n+1), expected_degree[2], 'b')

expected_degree = [[-1 for i in range(n)] for j in range(3)]
for i in range(n):
    sum = 0
    for j in range(n):
        sum += data_2[i][j]
        if sum > 0.1 and expected_degree[0][i] == -1:
            expected_degree[0][i] = j
        elif sum > 0.5 and expected_degree[1][i] == -1:
            expected_degree[1][i] = j
        elif sum > 0.9 and expected_degree[2][i] == -1:
            expected_degree[2][i] = j

plt.plot(range(1, n+1), expected_degree[0], 'ro')
plt.plot(range(1, n+1), expected_degree[1], 'go')
plt.plot(range(1, n+1), expected_degree[2], 'bo')

plt.legend(['10th', '50th', '90th'], loc='upper right')
plt.xlabel('vertex')
plt.ylabel('degree')
plt.grid(True)
plt.axis([1, n, -1, max(expected_degree[2])+1])
plt.title('d = %d' % d)
plt.savefig(output + '/vertex-degree_d%d.png' % d)
