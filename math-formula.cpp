#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>

using namespace std;

typedef unsigned long long ullong;
const string output_folder = "output";

template <typename T>
T choose(T n, T k) {
  if (k > n) {
	return 0;
  }
  T r = 1;
  for (T d = 1; d <= k; d++) {
	r *= n--;
	r /= d;
  }
  return r;
}

inline double correct_probability(double p) {
  if (p < 0)
	return 0;
  else if (p > 1)
	return 1;
  return p;
}

//------------------------------------------------------------
/// Calculate the probability that vertex v of graph G(n, d)
/// has degree of k
//------------------------------------------------------------
template <typename T>
double probability(T v, T k, T n, T d) {
  double result = 0;
  double p;
  for (T x = max(T(0), k-n+v); x <= min(k, v-1); x++) {
	p = correct_probability(d*1.0/v);
	double common = choose(v-1, x) * pow(p, x) * pow(1-p, v-1-x);
	if (n-v > 0) {
	  double sum = 0;
	  if (k == x) {
		double temp = 1;
		for (T i = v+1; i < n+1; i++)
		  temp *= correct_probability(1-d*1.0/i);
		sum = temp;
	  }
	  else {
		T list[n-v];
		for (T i = 0; i < n-v; i++)
		  list[i] = v+1+i;

		T combination[k-x];
		for (T i = 0; i < k-x; i++)
		  combination[i] = i;

		T i;
		do {
		  double temp = 1;
		  for (T r = 0; r < n-v; r++) {
			bool is_selected = false;
			for (T s = 0; s < k-x; s++)
			  if (r == combination[s]) {
				is_selected = true;
				break;
			  }
			p = correct_probability(d*1.0/list[r]);
			if (is_selected)
			  temp *= p;
			else
			  temp *= (1-p);
		  }
		  sum += temp;

		  i = k-x-1;
		  while (i >= 0 && combination[i] == n-v-k+x+i)
			i -= 1;
		  combination[i]++;
		  for (T j = i+1; j < k-x; j++)
			combination[j] = combination[j-1] + 1;
		} while (i >= 0);
	  }
	  result += common * sum;
	}
	else {
	  result += common;
	}
  }
  return result;
}

int main(int argc, char *argv[]) {
  /*
  int n, d;
  if (argc >= 3) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return 0;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return 0;
  }
  else {
	return 0;
  }
  cout << probability(n, d, 10, 1) << endl;
  */

  int n, d;
  if (argc >= 3) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return 0;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return 0;
  }
  else {
	return 0;
  }

  char filename[128];
  snprintf(filename, sizeof(filename), "n%d_d%d_math.txt", n, d);
  fstream fs;
  fs.open((output_folder + "/" + filename).c_str(), fstream::out);

  for (int v = 1; v < n+1; v++)
	for (int k = 0; k < n; k++)
	  fs << "(" << v << ", " << k << ") = " 
		 << fixed << setprecision(5) << probability(v, k, n, d) << endl;

  cout << "Output to file: " << output_folder << "/" << filename << endl;
  fs.close();
  
  return 0;
}
