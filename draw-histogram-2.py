import numpy as np
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import re

n = int(sys.argv[1])
d = int(sys.argv[2])
'''
filename = "attachment_degree_n%d_d%d" % (n, d)
count_1 = 0
data_1 = [0 for i in range(n)]
max_data_1 = [0 for i in range(n)]
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("output/" + f) as textFile:
                for line in textFile:
                    count_1 += 1
                    raw = line.split()
                    data_1 = [data_1[i] + int(raw[i]) for i in range(n)]
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)
'''

degree = 10
hist = []

filename = "barabasi_degree_n%d_d%d" % (n, d)
count_2 = 0
data_2 = [0 for i in range(n)]
max_data_2 = [0 for i in range(n)]
min_data_2 = [n for i in range(n)]
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("output/" + f) as textFile:
                for line in textFile:
                    count_2 += 1
                    raw = [int(e) for e in line.split()]
                    for i in range(n):
                        data_2[i] += raw[i]
                        if raw[i] > max_data_2[i]:
                            max_data_2[i] = raw[i]
                        if raw[i] < min_data_2[i]:
                            min_data_2[i] = raw[i]
                    hist.append(raw[degree])
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

output = './degree_distribution/n%d' % n
if not os.path.exists(output):
    os.mkdir(output)

average_data_2 = [data_2[i]*1.0/count_2 for i in range(n)]

percentile90_data_2 = []
increment = 10**6/count_2 if count_2 < 10**6 else 1
for i in range(0, n, increment):
    temp = []
    max = i+increment if i+increment < n else n
    for f in os.listdir("./output"):
        if f.find(filename) > -1:
            try:
                with open("output/" + f) as textFile:
                    for line in textFile:
                        raw = [int(e) for e in line.split()]
                        temp.append(raw[i:max])
            except IOError as e:
                print "I/O error({0}): {1}".format(e.errno, e.strerror)
    percentile90_data_2.extend(np.percentile(temp, 90, axis=0))

plt.clf()
plt.hist(hist, bins='auto', normed=True)
plt.xlabel('# nodes')
plt.ylabel('percentage')
plt.title('degree = %d' % degree)
plt.savefig(output + '/degree_%d.png' % degree)
#plt.show()

exact_degree_distribution = [2.0*d*(d+1)*n/(k*(k+1)*(k+2)) if k>0 else 0 for k in range(n)]

plt.figure(1)

plt.subplot(121)
plt.plot(range(n), max_data_2, 'r')
plt.plot(range(n), percentile90_data_2, 'g')
plt.plot(range(n), average_data_2, 'b')
plt.plot(range(n), min_data_2, 'y')
plt.plot(range(n), exact_degree_distribution, 'rx')
plt.legend(['max', 'percentile=90', 'average', 'min', 'exact degree distribution'], loc='upper right')
plt.xlabel('degree')
plt.ylabel('# nodes')
plt.grid(True)
plt.axis([1, 200, -1, 60])
plt.title('Linear scaling')

plt.subplot(122)
plt.loglog(range(n), max_data_2, 'r')
plt.loglog(range(n), percentile90_data_2, 'g')
plt.loglog(range(n), average_data_2, 'b')
plt.loglog(range(n), min_data_2, 'y')
plt.loglog(range(n), exact_degree_distribution, 'rx')
plt.xlabel('degree')
plt.ylabel('# nodes')
plt.grid(True)
plt.title('Log-log scaling')

#plt.savefig(output + '/degree-numOfNodes_d%d.png' % d)
plt.show()
