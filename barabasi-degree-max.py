import numpy as np
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import re

n = int(sys.argv[1])
d = int(sys.argv[2])

filename = "degree_max_n%d_d%d_" % (n, d)
#filename = "degree_n%d_d%d" % (n, d)
count = 0
data = np.zeros((n,), dtype=np.int)
for f in os.listdir("./barabasi_output"):
    if f.find(filename) > -1:
        try:
            with open("./barabasi_output/" + f) as textFile:
                for line in textFile:
                    count += 1
                    raw = [int(e) for e in line.split()]
                    processed = []
                    for i in xrange(0, len(raw), 2):
                        for j in xrange(raw[i+1]):
                            processed.append(raw[i])
                    data = np.add(data, processed)
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

output = './degree_distribution/n%d' % n
if not os.path.exists(output):
    os.mkdir(output)

if count > 1:
    data = np.multiply(data, 1.0/count)

#exact_degree_distribution = [2.0*d*(d+1)*n/(k*(k+1)*(k+2)) if k>0 else 0 for k in range(n)]

plt.figure(figsize=(24, 14))

plt.subplot(121)
plt.plot(range(n), data, 'b')
#plt.plot(range(n), exact_degree_distribution, 'rx')
#plt.legend(['experiment', 'exact degree distribution'], loc='upper right')
plt.xlabel('# nodes')
plt.ylabel('max degree')
plt.grid(True)
#plt.axis([1, 200, -1, 300])
plt.title('Linear scaling')

plt.subplot(122)
plt.loglog(range(n), data, 'b')
#plt.loglog(range(n), exact_degree_distribution, 'rx')
plt.xlabel('# nodes')
plt.ylabel('max degree')
plt.grid(True)
plt.title('Log-log scaling')

plt.savefig(output + '/max-degree_d%d.png' % d)
#plt.show()
