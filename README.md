# [Barabasi-Albert model](http://barabasi.com/networksciencebook/chapter/5)
There are two type of random graph: Random Attachment (new nodes link randomly to any nodes) and Preferential Attachment (new nodes prefer to link to the more connected nodes).

The Barabasi-Albert model is one kind of Preferential Attachment. When a new node `k` is added, one node `i` is connected to the new node with the probabily:

`P(i) = d * degree(i) / sum_degree(j: 0->k)` with `d is the parametter of Barabasi-Albert model`

## Degree distribution
In this assignment, we will find out the degree distribution of a Barabasi-Albert model graph in order to prove that the Barabasi-Albert model is scale-free, in particular, it is a power law of the form:

`P(k_degree) = k_degree^-3`

## Implementation
We implement two algorithms:

- Build a Barabasi-Albert model graph to get the adjacency list of that graph, then calculate the degree distribution. More details, see `random-graph.cpp`
- Build directly the degree distribution (when new node connects to the k-degree node, the number of k-degree nodes decrease 1, and the number of (k+1)-degree nodes increase 1). More details, see `degree-distribution.cpp`

## Running
### Build
```
g++ [-D BARABASI] -o random-graph random-graph.cpp 
```
Explanation

- `-D BARABASI` Set model of graph, by default `Random Attachment Model`

```
g++ -o degree-dist degree-distribution.cpp 
```

### Run
```
./random-graph n d t
```

```
./degree-dist n d t
```
Explanation

- `n, d` the parameters of the graph
- `t` the number of tries, by default `t = 1000`

## Result
Using `barabasi-degree-dist.py` and `barabasi-degree-max.py` to show the result of `degree-distribution.cpp`.
```
python barabasi-degree-dist.py n d
```

Using `draw-histogram[-1, 2, 3].py` to show the result of `random-graph.cpp`
```
python draw-histogram.py n d
```
