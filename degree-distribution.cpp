#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <dirent.h>

using namespace std;

const string output_folder = "barabasi_output";

//--------------------------------------------------------------------
/// Find out the degree distribution of Barabasi-Albert model graph
/// @param n, d: parameters of Random Graph
/// @param max_degree: the maximum degree
/// @return deg_dist: degree distribution
//--------------------------------------------------------------------
int *barabasi_degree_dist(int n, int d, fstream &fs) {
  int *deg_dist = new int[n];
  int *increasing_degree = new int[d];

  for (int i = 0; i < n; i++)
	deg_dist[i] = (i == 1 ? d : 0);
  deg_dist[d]++;

  int count = d;
  int current_max = 1;
  if (d > 1)
	fs << current_max << " " << count << " ";

  count = (d==1 ? 2 : 1);
  current_max = d;

  int num_links = d;
  for (int i = d+1; i < n; i++) {
	int selected_deg = 0;
	int range = 2*num_links;
	// link new node to d old nodes
	for (int j = 0; j < d; j++) {
	  range -= selected_deg;
	  int random = rand() % range;
#if defined DEBUG
	  cout << "Random = " << random << endl;
#endif
	  int cumulative = 0;
	  for (selected_deg = 1; selected_deg < i; selected_deg++) {
		cumulative += selected_deg * deg_dist[ selected_deg ];
		if (random < cumulative)
		  break;
	  }
#if defined DEBUG
	  cout << "Selected_DEG = " << selected_deg << endl;
#endif

	  // decrease #nodes have degree 'selected_deg'
	  deg_dist[ selected_deg ]--;
	  
	  // store 'selected_deg+1' in order to increase later
	  increasing_degree[j] = selected_deg+1;
	  if (selected_deg+1 > current_max) {
		fs << current_max << " " << count << " ";
		current_max = selected_deg+1;
		count = 1;
	  }
	  else {
		count++;
	  }
	}
	// increase #nodes have degree 'd'
	deg_dist[d]++;
	
	// increase #nodes have degree 'selected_deg+1'
	for (int j = 0; j < d; j++)
	  deg_dist[ increasing_degree[j] ]++;

	num_links += d;
  }
  fs << current_max << " " << count << endl;

  delete [] increasing_degree;
  return deg_dist;
}

int getOutputFileNumber(int n, int d) {
  DIR *dir;
  dirent *pdir;
  int t_max = 0;

  dir = opendir(("./" + output_folder).c_str());
  int n_temp, d_temp, t_temp;
  string filename_temp;
  while (pdir = readdir(dir)) {
	filename_temp = pdir->d_name;
	n_temp = d_temp = t_temp = 0;
	sscanf(filename_temp.c_str(), "degree_n%d_d%d_%d", &n_temp, &d_temp, &t_temp);
	if (n == n_temp && d == d_temp)
	  if (t_temp > t_max) {
		t_max = t_temp;
	  }
  }
  return t_max+1;
}

//--------------------------------------------------------------------
/// Process parameters
/// @param argc, argv: input parameters
/// @param n, d: parameters of Random Graph
/// @param t: the number of tries
/// @return true if successful
///         false otherwise
//--------------------------------------------------------------------
bool processInput(int argc, char *argv[], int &n, int &d, int &t) {
  if (argc >= 3) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return false;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return false;
  }
  else {
	return false;
  }
  if (argc >= 4) {
	istringstream iss3( argv[3] );
    if (!(iss3 >> t))
	  return false;
  }
  return true;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int n, d, tries = 1000;
  // process input
  if (processInput(argc, argv, n, d, tries) == false)
	return 0;

  int fileNumber = getOutputFileNumber(n, d);
  char filename1[128], filename2[128];

  snprintf(filename1, sizeof(filename1), "degree_n%d_d%d_%d.txt", n, d, fileNumber);
  snprintf(filename2, sizeof(filename2), "degree_max_n%d_d%d_%d.txt", n, d, fileNumber);
  
  fstream fs1, fs2;
  fs1.open((output_folder + "/" + filename1).c_str(), fstream::out | fstream::app);
  fs2.open((output_folder + "/" + filename2).c_str(), fstream::out | fstream::app);

  for (int i = 0; i < tries; i++) {
	int *result = barabasi_degree_dist(n, d, fs2);
	for (int i = 0; i < n; i++) {
	  if (result[i] > 0)
		fs1 << i << " " << result[i] << " ";
	}
	fs1 << endl;
	delete [] result;
  }

  fs1.close();
  fs2.close();
  return 0;
}
