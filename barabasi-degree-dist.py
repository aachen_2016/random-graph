import numpy as np
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import re

n = int(sys.argv[1])
d = int(sys.argv[2])

filename = "degree_n%d_d%d_" % (n, d)
count = 0
data = [0] * n
max_data = [0] * n
min_data = [n] * n
for f in os.listdir("./barabasi_output"):
    if f.find(filename) > -1:
        try:
            with open("./barabasi_output/" + f) as textFile:
                for line in textFile:
                    count += 1
                    raw = [int(e) for e in line.split()]
                    processed = [0] * n
                    for x in xrange(0, len(raw), 2):
                        processed[raw[x]] = raw[x+1]
                    data = np.add(data, processed)
                    max_data = np.maximum(max_data, processed)
                    min_data = np.minimum(min_data, processed)
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

output = './degree_distribution/n%d' % n
if not os.path.exists(output):
    os.mkdir(output)

average_data = np.multiply(data, 1.0/count)

percentile90_data = []
increment = 10**6/count if count < 10**6 else 1
for i in range(0, n, increment):
    temp = []
    max = i+increment if i+increment < n else n
    for f in os.listdir("./barabasi_output"):
        if f.find(filename) > -1:
            try:
                with open("./barabasi_output/" + f) as textFile:
                    for line in textFile:
                        raw = [int(e) for e in line.split()]
                        processed = [0] * n
                        for x in xrange(0, len(raw), 2):
                            processed[raw[x]] = raw[x+1]
                        temp.append(processed[i:max])
            except IOError as e:
                print "I/O error({0}): {1}".format(e.errno, e.strerror)
    percentile90_data.extend(np.percentile(temp, 90, axis=0))

exact_degree_distribution = [2.0*d*(d+1)*n/(k*(k+1)*(k+2)) if k>0 else 0 for k in xrange(n)]

plt.figure(figsize=(24, 14))

plt.subplot(121)
x_axis = xrange(n)
plt.plot(x_axis, max_data, 'r')
plt.plot(x_axis, percentile90_data, 'g')
plt.plot(x_axis, average_data, 'b')
plt.plot(x_axis, min_data, 'y')

#plt.plot(x_axis, data, 'b')
plt.plot(x_axis, exact_degree_distribution, 'rx')
plt.legend(['max', 'percentile=90', 'average', 'min', 'exact degree distribution'], loc='upper right')
#plt.legend(['experiment', 'exact degree distribution'], loc='upper right')
plt.xlabel('degree')
plt.ylabel('# nodes')
plt.grid(True)
plt.axis([1, 200, -1, 200])
plt.title('Linear scaling')

plt.subplot(122)

plt.loglog(x_axis, max_data, 'r')
plt.loglog(x_axis, percentile90_data, 'g')
plt.loglog(x_axis, average_data, 'b')
plt.loglog(x_axis, min_data, 'y')

#plt.loglog(x_axis, data, 'b')
plt.loglog(x_axis, exact_degree_distribution, 'rx')
plt.xlabel('degree')
plt.ylabel('# nodes')
plt.grid(True)
plt.axis([0, 10**4, 0.1, 10**6])
plt.title('Log-log scaling')

plt.savefig(output + '/degree-distribution_d%d.png' % d)
#plt.show()
