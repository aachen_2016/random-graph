import numpy as np
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import re

n = int(sys.argv[1])
d = int(sys.argv[2])
'''
filename = "attachment_degree_n%d_d%d" % (n, d)
data_1 = []
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("output/" + f) as textFile:
                for line in textFile:
                    raw = [int(e) for e in line.split()]
                    data_1.append(raw)
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)
data_1 = np.array(data_1)

max_data_1 = data_1.max(axis=0)
min_data_1 = data_1.min(axis=0)
average_data_1 = np.average(data_1, axis=0)
'''

filename = "barabasi_degree_n%d_d%d" % (n, d)
data_2 = []
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("output/" + f) as textFile:
                for line in textFile:
                    raw = [int(e) for e in line.split()]
                    data_2.append(raw)
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)
data_2 = np.array(data_2)

max_data_2 = data_2.max(axis=0)
min_data_2 = data_2.min(axis=0)
average_data_2 = np.average(data_2, axis=0)
percentile90_data_2 = np.percentile(data_2, 90, axis=0)

output = './degree_distribution/n%d' % n
if not os.path.exists(output):
    os.mkdir(output)

plt.figure(1)

plt.subplot(121)
plt.plot(range(n), max_data_2, 'r')
plt.plot(range(n), percentile90_data_2, 'g')
plt.plot(range(n), average_data_2, 'b')
plt.plot(range(n), min_data_2, 'y')
plt.legend(['max', 'percentile=90', 'average', 'min'], loc='upper right')
plt.xlabel('degree')
plt.ylabel('# node')
plt.grid(True)
plt.axis([1, 200, -1, 60])
plt.title('Linear scaling')

plt.subplot(122)
plt.loglog(range(n), max_data_2, 'r')
plt.loglog(range(n), percentile90_data_2, 'g')
plt.loglog(range(n), average_data_2, 'b')
plt.plot(range(n), min_data_2, 'y')
plt.xlabel('degree')
plt.ylabel('# node')
plt.grid(True)
plt.title('Log-log scaling')

#plt.savefig(output + '/degree-numOfNodes_d%d.png' % d)
plt.show()

degree = 10
hist = data_2[:, degree]
plt.hist(hist, bin='auto', normed=True)
plt.xlabel('# nodes')
plt.ylabel('percentage')
plt.title('degree = %d' % degree)
plt.savefig(output + '/degree_d%d.png' % degree)
#plt.show()
