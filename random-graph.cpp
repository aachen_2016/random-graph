#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <list>
#include <vector>
#include <queue>
#include <dirent.h>

using namespace std;

const string output_folder = "output";

class RandomGraph {
protected:
  int n; // number of vertices
  int d; // parameter used to check (u, v) is an edge or not
  list<int> *adj; // adjacency list

public:
  RandomGraph(int n, int d = 1) {
	this->n = n;
	this->d = (d < 1 || d >= n) ? 1 : d;
	this->adj = new list<int>[n];
  }
  ~RandomGraph() {
	delete [] adj;
  }

  // Add new edge to the graph
  void add_edge(int u, int v) {
	adj[u].push_back(v);
	adj[v].push_back(u);
  }

  // Display the graph
  void display();

  // Get degree of a vertex
  int get_degree(int v);

  // Get degree histogram of the graph
  int *get_degree_hist();

  // Get distance histogram of the graph
  int *get_distance_hist();

  // Get distance matrix of the graph
  int **get_distance_matrix();

private:
  virtual void generate() = 0;
};

class RandomAttachment : public RandomGraph {
public:
  RandomAttachment(int n, int d) : RandomGraph(n, d) {
	generate();
  }

private:
  // Generate random graph
  void generate() {
	for (int i = 1; i < n; i++) {
	  for (int j = 0; j < i; j++) {
		if (rand() % (i+1) + 1 <= d)
		  add_edge(i, j);
	  }
	}
  }  
};

//------------------------------------
/// Get d unique elements from l
//------------------------------------
list<int> random_subset(vector<int> l, int d) {
  list<int> result;
  while (result.size() < d) {
	result.push_back( l[rand() % l.size()] );
	result.sort();
	result.unique();
  }
  return result;
}

class BarabasiAlbert : public RandomGraph {
public:
  BarabasiAlbert(int n, int d) : RandomGraph(n, d) {
	generate();
  }

private:
  // Generate random graph
  void generate() {
	vector<int> repeated_nodes;

	list<int> targets;
	for (int i = 0; i < d; i++)
	  targets.push_back(i);

	list<int>::iterator it;
	for (int i = d; i < n; i++) {
	  for (it = targets.begin(); it != targets.end(); it++) {
		add_edge(i, *it);
		repeated_nodes.push_back(*it);
	  }
	  repeated_nodes.push_back(i);
	  targets = random_subset(repeated_nodes, d);
	}
  }
};

//--------------------------------------------------
/// Display the graph
//--------------------------------------------------
void RandomGraph::display() {
  for (int vertex = 0; vertex < n; vertex++) {
	list<int>::iterator it;
	for (it = adj[vertex].begin(); it != adj[vertex].end(); it++)
	  cout << vertex << " " << *it << endl;
  }
}

//--------------------------------------------------
/// Get degree of a vertex
//--------------------------------------------------
int RandomGraph::get_degree(int v) {
  return adj[v].size();
}

//--------------------------------------
/// Get degree histogram of the graph
//--------------------------------------
int *RandomGraph::get_degree_hist() {
  int *hist = new int[n];
  for (int i = 0; i < n; i++)
	hist[i] = 0;
  for (int i = 0; i < n; i++)
	hist[ adj[i].size() ]++;
  return hist;
}

//----------------------------------------
/// Get distance histogram of the graph
//----------------------------------------
int *RandomGraph::get_distance_hist() {
  int **distance = get_distance_matrix();
  int *hist = new int[n];
  for (int i = 0; i < n; i++)
	hist[i] = 0;

  for (int i = 0; i < n; i++)
	for (int j = 0; j < i; j++)
	  if (distance[i][j] > 0)
		hist[ distance[i][j] ]++;

  for (int i = 0; i < n; i++)
	delete [] distance[i];
  delete [] distance;

  return hist;
}

//--------------------------------------------
/// Get distance matrix of the graph
//--------------------------------------------
int **RandomGraph::get_distance_matrix() {
  int **distance = new int *[n];
  for (int i = 0; i < n; i++)
	distance[i] = new int[n];

  // initialize distance matrix without any connection
  for (int i = 0; i < n; i++)
	for (int j = 0; j < n; j++)
	  distance[i][j] = (i == j) ? 0 : -1;

  // run BFS algorithm to build distance matrix
  for (int i = 0; i < n; i++) {
	queue<int> Q;
	Q.push(i);
	while (!Q.empty()) {
	  int current = Q.front();
	  Q.pop();
	  for (list<int>::iterator it = adj[current].begin(); it != adj[current].end(); it++)
		if (distance[i][*it] == -1) {
		  distance[i][*it] = distance[i][current] + 1;
		  Q.push(*it);
		}
	}
  }

#if defined DEBUG
  for (int i = 0; i < n; i++) {
	for (int j = 0; j < n; j++)
	  cout << distance[i][j] << " ";
	cout << endl;
  }
#endif

  return distance;
}

//--------------------------------------------------------------------
/// Process parameters
/// @param argc, argv: input parameters
/// @param n, d: parameters of Random Graph
/// @param t: the number of tries
/// @return true if successful
///         false otherwise
//--------------------------------------------------------------------
bool processInput(int argc, char *argv[], int &n, int &d, int &t) {
  if (argc >= 3) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return false;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return false;
  }
  else {
	return false;
  }
  if (argc >= 4) {
	istringstream iss3( argv[3] );
    if (!(iss3 >> t))
	  return false;
  }
  return true;
}

int getOutputFileNumber(int n, int d) {
  DIR *dir;
  dirent *pdir;
  int t_max = 0;

  dir = opendir(("./" + output_folder).c_str());
  int n_temp, d_temp, t_temp;
  string filename_temp;
  while (pdir = readdir(dir)) {
	filename_temp = pdir->d_name;
	n_temp = d_temp = t_temp = 0;
#if defined BARABASI
	sscanf(filename_temp.c_str(), "barabasi_degree_n%d_d%d_%d", &n_temp, &d_temp, &t_temp);
#else
	sscanf(filename_temp.c_str(), "attachment_degree_n%d_d%d_%d", &n_temp, &d_temp, &t_temp);
#endif
	if (n == n_temp && d == d_temp)
	  if (t_temp > t_max) {
		t_max = t_temp;
	  }
  }
  return t_max+1;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int n, d, tries = 1000;

  // process input
  if (processInput(argc, argv, n, d, tries) == false)
	return 0;

  int fileNumber = getOutputFileNumber(n, d);
  char filename1[128], filename2[128], filename3[128];

#if defined BARABASI
  snprintf(filename1, sizeof(filename1), "barabasi_degree_n%d_d%d_%d.txt", n, d, fileNumber);
  snprintf(filename2, sizeof(filename2), "barabasi_distance_n%d_d%d_%d.txt", n, d, fileNumber);
  snprintf(filename3, sizeof(filename3), "barabasi_vertex_degree_n%d_d%d_%d.txt", n, d, fileNumber);
#else
  snprintf(filename1, sizeof(filename1), "attachment_degree_n%d_d%d_%d.txt", n, d, fileNumber);
  snprintf(filename2, sizeof(filename2), "attachment_distance_n%d_d%d_%d.txt", n, d, fileNumber);
  snprintf(filename3, sizeof(filename3), "attachment_vertex_degree_n%d_d%d_%d.txt", n, d, fileNumber);
#endif

  fstream fs1, fs2, fs3;
  fs1.open((output_folder + "/" + filename1).c_str(), fstream::out | fstream::app);
  fs2.open((output_folder + "/" + filename2).c_str(), fstream::out | fstream::app);
  fs3.open((output_folder + "/" + filename3).c_str(), fstream::out | fstream::app);
  
  for (int t = 0; t < tries; t++) {
#if defined BARABASI
	RandomGraph *g = new BarabasiAlbert(n, d);
#else
	RandomGraph *g = new RandomAttachment(n, d);
#endif

#if defined DEBUG
	g->display();
#endif
	
	int *hist = g->get_degree_hist();
	for (int i = 0; i < n; i++)
	  fs1 << hist[i] << " ";
	fs1 << endl;
	delete [] hist;
	
	hist = g->get_distance_hist();
	for (int i = 0; i < n; i++)
	  fs2 << hist[i] << " ";
	fs2 << endl;
	delete [] hist;
	
	for (int v = 0; v < n; v++)
	  fs3 << g->get_degree(v) << " ";
	fs3 << endl;
	
	delete g;
  }
  cout << "Output to file: " << output_folder << "/" << filename1 << endl;
  fs1.close();
  fs2.close();
  fs3.close();

  return 0;
}
